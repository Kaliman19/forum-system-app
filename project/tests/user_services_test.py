import unittest
from unittest.mock import Mock, patch
from fastapi import HTTPException
from services import user_services

from data.models import BaseUser
from data.utils import _hash

#TODO vts/ 03-05-2023:
# There are tests, which do not work if I start them
# as part of the whole test-suite; to be rewritten !

class UserServices_Should(unittest.TestCase):
    def setUp(self) -> None:
        self.pswrd = _hash("vesko123")
        self.params = [
            (
                1,
                "Vesko",
                "vesko@abv.bg",
                self.pswrd,
                "Vesselin",
                "Tsvetanov",
                "+359897122227",
                True,
            )
        ]

        self._get_func = lambda id: self.params

    def tearDown(self) -> None:
        for k, v in vars(self).items():
            setattr(self, k, None)

    def test_get_user_by_id_with_int(self):
        with patch("services.user_services.database") as mock_db:
            mock_db.read_query.side_effect = [self._get_func(1)]*4
            result = user_services.get_user_by_id(1, get_func=self._get_func)
            expected = user_services.get_user_by_id(1)

            self.assertEqual(result, expected)

    def test_get_user_by_id_with_str(self):
        with patch("services.user_services.database") as mock_db:
            mock_db.read_query.side_effect = [self._get_func("vesko")]*2

            result = user_services.get_user_by_id("vesko", get_func=self._get_func)
            expected = user_services.get_user_by_id("vesko")

            self.assertEqual(result, expected)

    def test_get_user_by_id_with_read_query_None(self):
        with patch("services.user_services.database") as mock_db:
            mock_db.read_query.return_value = None

            result = user_services.get_user_by_id("vesko")
            expected = None

            self.assertEqual(result, expected)

    def test_get_user_by_id_with_not_allowed_data(self):
        result = user_services.get_user_by_id(object())
        expected = None

        self.assertEqual(result, expected)

    def test_user_exists(self):
        with patch("services.user_services.get_user_by_id") as mock_db:
            mock_db.return_value = True
            f = lambda id: True
            result = user_services.user_exists(1)
            expected = user_services.user_exists(1, f)

            self.assertEqual(result, expected)

    def test_create_user_with_correct_parameters(self):
        baseuser = BaseUser.create_db_user(*self.params[0][1:])
        with patch("services.user_services.database") as mock_db:
            mock_db.insert_query.return_value = 1
            mock_db.read_query.return_value = self.params

            _insert_func = lambda x: 1
            _get_row_func = lambda x: self.params

            result = user_services.create_user(baseuser)
            expected = user_services.create_user(baseuser, _insert_func, _get_row_func)

            self.assertEqual(result, expected)

    def test_get_all_users_no_logged_user_raisesException(self):
        with self.assertRaises(HTTPException):
            logged_user = lambda: 0 # irrelevant, used only for currier
            logged_user.is_admin = False
            user_services.get_all_users(logged_user)

    def test_get_all_users_with_logged_user(self):
        logged_user = lambda: 0  # irrelevant, used only for currier
        logged_user.is_admin = True

        with patch("services.user_services.database") as mock_db:
            mock_db.read_query.return_value = self.params

            _get_all = lambda: self.params

            result = user_services.get_all_users(logged_user)
            expected = user_services.get_all_users(logged_user, _get_all)

            self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()
