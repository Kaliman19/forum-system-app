import unittest
from unittest.mock import patch
from datetime import datetime, timedelta
from services import topic_services
from data.models import Topic,\
                        Reply, \
                        TopicResponse, \
                        TopicResponseForCategory, \
                        TopicResponseByUsername

from dataclasses import dataclass
from operator import itemgetter

from functools import partial

f_date = lambda t: t.create_date
f_title = lambda t: t.title
f_id = lambda t: t.id

rev = False

sf_date = partial(sorted, key=f_date, reverse=rev)
sf_title = partial(sorted, key=f_title, reverse=rev)
sf_id = partial(sorted, key=f_id, reverse=rev)


@dataclass
class dummy_user:
    id: int
    username:str


class TopicServices_Should(unittest.TestCase):
    def setUp( self ) -> None:
        self.topic_params = [
            [
               1,
               "Title: first topic",
               "Description: good ** 3",
                datetime.now(),
                1,
                "Vesko",
                False
            ]
        ]

        self.topic_response_params = [
            (
                *self.topic_params,
                [Reply.from_query_result(
                    1,
                    "what a fantastic reply",
                    datetime.now(),
                    1,
                    1,
                    5,
                    1
                )]
            )
        ]

        self.logged_user = dummy_user(1, "dummy")

        d = datetime.now()

        self.topics = [Topic.from_query_result(*val) for val in [(1, "A", "desc A",              d, 1, "dymmy", 1),
                                                                 (2, "B", "desc B", d+timedelta(1), 1, "dymmy", 1),
                                                                 (3, "C", "desc C", d+timedelta(2), 1, "dymmy", 1)]]

    def tearDown(self) -> None:
        for k, v in vars(self).items():
            setattr(self, k, None)

    def test_all_topics_search_is_None( self ):
        with patch("services.topic_services.read_query") as mock_db:
            mock_db.return_value = [self.topic_params[0][:-1]]

            result = next(topic_services.all_topics_by_user(None))
            expected = TopicResponseByUsername.respondbyusername(*self.topic_params[0][:-1])

            self.assertEqual(result, expected)

    def test_all_topics_search_is_str( self ):
        with patch("services.topic_services.read_query") as mock_db:
            mock_db.return_value = [self.topic_params[0][:-1]]

            result = next(topic_services.all_topics_by_user("Something"))
            expected = TopicResponseByUsername.respondbyusername(*self.topic_params[0][:-1])

            self.assertEqual(result, expected)

    def test_get_by_id_with_int( self ):
        with patch("services.topic_services.read_query") as mock_db:
            mock_db.return_value = self.topic_params

            result = topic_services.get_by_id(self.topic_params[0][0])
            expected = TopicResponse.topic_response(*self.topic_params[0])
            self.assertEqual(result, expected)

    def test_get_by_category_with_int( self ):
        with patch("services.topic_services.read_query") as mock_db:
            ig = itemgetter(0,1,2,3,5)

            mock_db.return_value = [ig(self.topic_params[0])]
            result = next(topic_services.get_by_category(self.topic_params[0][0]))
            expected = TopicResponseForCategory.for_category(*ig(self.topic_params[0]))
            self.assertEqual(result, expected)

    def test_create_with_correct_parameters( self ):
        with patch("services.topic_services.first_row_query") as mock_db:

            mock_db.side_effect = [self.topic_params[0]]
            topic_dummy = Topic.from_query_result(*self.topic_params[0])
            result = topic_services.create(topic_dummy, self.logged_user)
            expected = topic_dummy
            self.assertEqual(result, expected)

    def test_to_sort_attr_time( self ):
        result = topic_services.to_sort(self.topics, attribute="time")
        expected = sf_date(self.topics)

        self.assertEqual(result, expected)

    def test_to_sort_attr_title( self ):
        result = topic_services.to_sort(self.topics, attribute="title")
        expected = sf_title(self.topics)

        self.assertEqual(result, expected)

    def test_to_sort_attr_else( self ):
        result = topic_services.to_sort(self.topics, attribute="nothing")
        expected = sf_id(self.topics)

        self.assertEqual(result, expected)

    def test_exist_with_int_return_False( self ):
        with patch("services.topic_services.read_query") as mock_db:
            mock_db.return_value = []

            result = topic_services.exist(1)
            expected = False

            self.assertEqual(result, expected)

    def test_exist_with_int_return_True( self ):
        with patch("services.topic_services.read_query") as mock_db:
            mock_db.return_value = [1]

            result = topic_services.exist(1)
            expected = True

            self.assertEqual(result, expected)

    def test_delete_topic_by_id(self):
        with patch("services.topic_services.update_query") as mock_db:
            mock_db.return_value = self.topic_params[0]

            t = lambda x: 0
            t.id = None

            result = topic_services.delete(t)
            expected = self.topic_params[0]

            self.assertEqual(result, expected)

    def test_update_topic(self):
        old_topic = Topic.from_query_result(*self.topic_params[0])

        new_params = {"description": "oh i'm new", "is_locked":0}
        expected = self.topic_params[0].copy()
        expected[2] = new_params["description"]
        expected[-1] = new_params["is_locked"]

        with patch("services.topic_services.update_query") as mock_db:
            mock_db.return_value = expected
            result = topic_services.update(old_topic, new_params)

            self.assertEqual(result, expected)

    def test_all_topics_by_user_no_search_criteria( self ):
        #TODO: to be clarified ...
        pass

    def test_all_topics_by_user_with_search_criteria( self ):
        with patch("services.topic_services.read_query") as mock_db:
            row = [self.topic_params[0][:-1]]
            mock_db.return_value = row

            result = next(topic_services.all_topics_by_user("dummy"))
            expected = TopicResponseByUsername.respondbyusername(*row[0])

            self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()

