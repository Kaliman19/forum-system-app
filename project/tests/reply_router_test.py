import unittest
from unittest.mock import Mock,patch
from fastapi import HTTPException
from data.utils import _hash
from routers import reply_routes
from data.models import DBUser

mock_reply_services=Mock(spec='services.reply_services')
reply_routes.reply_services=mock_reply_services

def fake_reply():
    reply = Mock()

    return reply
def fake_dbuser(
    id=1,
    username="Vesko",
    email="vesko@abv.bg",
    password=_hash("vesko123"),
    first_name="Vesselin",
    last_name="Tsvetanov",
    phone="+359897122227",
    is_admin=True,
):

    mock_dbuser = Mock(spec=DBUser)
    mock_dbuser.id = id
    mock_dbuser.username = username
    mock_dbuser.email = email
    mock_dbuser.password = password
    mock_dbuser.first_name = first_name
    mock_dbuser.last_name = last_name
    mock_dbuser.phone = phone
    mock_dbuser.is_admin = is_admin

    return mock_dbuser
class ReplyRouter_should_test(unittest.TestCase):
    def setUp(self) -> None:
       mock_reply_services.reset_mock()

    # def tearDown(self) -> None:
    #     for k, v in vars(self).items():
    #         setattr(self, k, None)

    def test_get_replies_Raises_error_ifNot_Admin(self):
        user=fake_dbuser()
        user.is_admin=False
        with self.assertRaises(HTTPException):
            reply_routes.get_replies(user)

    def test_get_replies_returnsReplies_if_Admin(self):
        reply1=fake_reply()
        reply2=fake_reply()
        test_replies=[reply1,reply2]
        user = fake_dbuser()
        mock_reply_services.all=lambda :test_replies
        result=reply_routes.get_replies(user)
        self.assertEqual(test_replies, result)

    def test_get_replyBy_Id_Raises_error_ifNot_Admin(self):
        user = fake_dbuser()
        user.is_admin = False
        with self.assertRaises(HTTPException):
            reply_routes.get_reply_by_id(1)

    def test_get_replyBy_Id_returnsReply_if_Admin(self):
        reply1 = fake_reply()
        id=1
        user = fake_dbuser()
        user.is_admin = False
        mock_reply_services.get_by_id = lambda q: reply1
        result = reply_routes.get_reply_by_id(id)
        expected=reply1
        self.assertEqual(expected, result)
