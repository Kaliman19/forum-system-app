import unittest
from unittest.mock import Mock
from fastapi import HTTPException
from data.models import DBUser

from data.utils import _hash

from routers import user as user_router


mock_user_services = Mock(spec='services.user_services')
user_router.user_services = mock_user_services


def fake_dbuser(
    id=1,
    username="Vesko",
    email="vesko@abv.bg",
    password=_hash("vesko123"),
    first_name="Vesselin",
    last_name="Tsvetanov",
    phone="+359897122227",
    is_admin=True,
):

    mock_dbuser = Mock(spec=DBUser)
    mock_dbuser.id = id
    mock_dbuser.username = username
    mock_dbuser.email = email
    mock_dbuser.password = password
    mock_dbuser.first_name = first_name
    mock_dbuser.last_name = last_name
    mock_dbuser.phone = phone
    mock_dbuser.is_admin = is_admin

    return mock_dbuser


class UserRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_user_services.reset_mock()

    def test_create_user_errorInDB_raises_Error( self ):
        db_user = fake_dbuser()
        mock_user_services.create_user = lambda user: None
        with self.assertRaises(HTTPException):
            user_router.create_user(db_user)

    def test_get_all_registered_users( self ):
        # alredy exhaustively tested in user_services_test.py
        pass

    def test_get_user_no_such_user_raisesError( self ):
        db_user = fake_dbuser()
        mock_user_services.get_user_by_id = lambda username_or_id: None
        with self.assertRaises(HTTPException):
            user_router.get_user(db_user.username)

    def test_get_all_topics_by_user_no_admin_raisesError( self ):
        db_user = fake_dbuser()
        db_user.is_admin = False
        with self.assertRaises(HTTPException):
            user_router.get_all_topics_by_user(db_user.username, db_user)

    def test_get_all_topics_by_user_no_user_inDB_raisesError( self ):
        db_user = fake_dbuser()
        mock_user_services.get_user_by_id = lambda username_or_id: None
        with self.assertRaises(HTTPException):
            user_router.get_all_topics_by_user(db_user.username, db_user)

if __name__ == '__main__':
    unittest.main()