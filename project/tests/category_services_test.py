import unittest
from unittest.mock import patch
from data.models import Category
from services import category_services


class CategoryServices_Should(unittest.TestCase):
    def setUp(self) -> None:
        self.cat_params = [
            (
                1,
                "Some category",
                1,
                1
            )
        ]

    def tearDown(self) -> None:
        for k, v in vars(self).items():
            setattr(self, k, None)

    def test_all_categories_with_str( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = self.cat_params

            result = next(category_services.all_categories(""))
            expected = Category.category(*self.cat_params[0])

            self.assertEqual(result, expected)

    def test_all_categories_with_None( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = self.cat_params

            result = next(category_services.all_categories(None))
            expected = Category.category(*self.cat_params[0])

            self.assertEqual(result, expected)

    def test_get_by_id_with_correct_id( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = self.cat_params

            result = category_services.get_by_id(1)
            expected = Category.category(*self.cat_params[0])

            self.assertEqual(result, expected)

    def test_get_by_id_with_None( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = []

            result = category_services.get_by_id(1)
            expected = None

            self.assertEqual(result, expected)

    def test_get_by_name_with_correct_name( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = self.cat_params

            result = category_services.get_by_id("no_name")
            expected = Category.category(*self.cat_params[0])

            self.assertEqual(result, expected)

    def test_get_by_name_with_None( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = []

            result = category_services.get_by_id("no_name")
            expected = None

            self.assertEqual(result, expected)

    def test_exists_with_correct_id_returns_True( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = [1]

            result = category_services.exist(1)
            expected = True

            self.assertEqual(result, expected)

    def test_exists_with_correct_id_returns_False( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = []

            result = category_services.exist(1)
            expected = False

            self.assertEqual(result, expected)

    def test_exists_name_with_correct_name_returns_True( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = [1]

            result = category_services.exist_name("no_name")
            expected = True

            self.assertEqual(result, expected)

    def test_exists_name_with_correct_name_returns_False( self ):
        with patch("services.category_services.read_query") as mock_db:
            mock_db.return_value = []

            result = category_services.exist_name("no_name")
            expected = False

            self.assertEqual(result, expected)

    def test_create_with_correct_category( self ):
        cat = Category.category(*self.cat_params[0])

        with patch("services.category_services.insert_query") as mock_db:
            mock_db.return_value = 1

            result = category_services.create(cat, None)
            expected = cat

            self.assertEqual(result, expected)

    def test_delete_with_correct_id( self ):
        # TODO: need to think about it ...
        pass

    def test_update_with_correct_parametrs( self ):
        #TODO: need to think about it ...
        pass

if __name__ == '__main__':
    unittest.main()