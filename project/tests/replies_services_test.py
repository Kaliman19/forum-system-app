import unittest
from datetime import datetime
from unittest.mock import patch

from data.models import Reply, Createreply,Category
from services import reply_services

class ReplyRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        _date = datetime.now()

        self.reply_params = [
            (
                1,
                "some reply",
                _date,
                1,
                1,
                0,
                0
            )
        ]
        self.reply_obj = Reply.from_query_result(*self.reply_params[0])

    def tearDown(self) -> None:
        for k, v in vars(self).items():
            setattr(self, k, None)

    def test_all( self ):
        with patch("services.reply_services.read_query") as mock_db:
            mock_db.return_value = self.reply_params

            result = next(reply_services.all())
            expected = self.reply_obj

            self.assertEqual(result, expected)

    def test_get_by_id_with_correct_id( self ):
        with patch("services.reply_services.read_query") as mock_db:
            mock_db.return_value = self.reply_params
            _id = self.reply_params[0][0]
            result = reply_services.get_by_id(_id)
            expected = self.reply_obj

            self.assertEqual(result, expected)

    def test_get_by_topic_with_correct_topic_id( self ):
        with patch("services.reply_services.read_query") as mock_db:
            mock_db.return_value = self.reply_params
            _id = 1
            result = next(reply_services.get_by_topic(_id))
            expected = self.reply_obj

            self.assertEqual(result, expected)

    def test_exist_with_correct_id( self ):
        with patch("services.reply_services.read_query") as mock_db:
            mock_db.return_value = self.reply_params
            _id = 1

            result = reply_services.exist(_id)

            self.assertEqual(result, 1)

    def test_create_with_correct_Createreply( self ):
        with patch("services.reply_services.insert_query") as mock_db:
            _params = self.reply_params[0][:5]
            create_reply = Createreply.from_query_result(*_params)

            mock_db.return_value = _params

            result = reply_services.create(create_reply)
            expected = create_reply

            self.assertEqual(result, expected)

    def test_create_response_object_with_correct_topic_and_topic_replies( self ):
        pass

    def test_update_with_correct_id_and_reply( self ):
        with patch("services.reply_services.update_query") as mock_db:
            mock_db.return_value = self.reply_params

            result = reply_services.update(1, self.reply_obj)
            expected = self.reply_params

            self.assertEqual(result, expected)

    def test_delete_with_correct_id( self ):
        with patch("services.reply_services.update_query") as mock_db:
            mock_db.return_value = 1

            result = reply_services.delete(1)

            self.assertEqual(result, 1)

    def test_update_vote_with_correct_id_and_vote_id( self ):
        with patch("services.reply_services.update_query") as mock_db:
            mock_db.return_value = 1

            result = reply_services.update_vote(1,1)

            self.assertEqual(result, 1)

    def test_get_category_by_reply_with_correct_id( self ):
        with patch("services.reply_services.read_query") as mock_db:
            cat_params = [
                (
                    1,
                    "Some category",
                    1,
                    1
                )
            ]

            mock_db.return_value = cat_params

            result = reply_services.get_category_by_reply(1)
            expected = Category.category(*cat_params[0])

            self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()

