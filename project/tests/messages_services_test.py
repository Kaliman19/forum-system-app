# from dataclasses import dataclass
# import unittest
# from unittest.mock import Mock, patch
# from datetime import datetime
# from fastapi import HTTPException
# from services import messages_services
# from services import user_services
#
# from data.models import Message, DBMessage
#
#
# mock_user_services = Mock(spec='services.user_services')
# user_services.get_user_by_id = mock_user_services
#
#
# @dataclass
# class dummy_user:
#     id: int
#     username:str
#
#
# class MessagesServices_Should(unittest.TestCase):
#     def setUp(self) -> None:
#         self.params = [
#             (
#                 1,
#                 "Vesko",
#                 "vesko@abv.bg",
#                  "Vesselin",
#                 "Tsvetanov",
#                 True,
#             )
#         ]
#
#         self.msg = [
#             (
#                 1,
#                 "... some text ...",
#                 datetime.now(),
#                 self.params[0][0],
#                 self.params[0][0] + 1,
#             )
#         ]
#
#         self.logged_user = dummy_user(self.params[0][0],
#                                       self.params[0][1])
#         self.sender = self.logged_user
#         self.receiver = dummy_user(self.params[0][0] + 1,
#                                    "dummy")
#
#         self.msg_txt = Message.msg_create(self.msg[0][0],
#                                           self.msg[0][1])
#
#         self._get_func = lambda id: self.params
#
#
#
#     def tearDown(self) -> None:
#         for k, v in vars(self).items():
#             setattr(self, k, None)
#
#         mock_user_services.reset_mock()
#
#     def test_get_all_messaged_users_with_correct_data( self ):
#         mock_user_services.side_effect = [self.sender]*2
#
#         with patch("services.messages_services.database") as mock_db:
#             mock_db.read_query.return_value = self.params
#
#             result = messages_services.get_all_messaged_users(self.logged_user)
#             expected = messages_services.get_all_messaged_users(self.logged_user, self._get_func)
#
#             self.assertEqual(result, expected)
#
#     def test_get_conversation_with_user_with_correct_data( self ):
#         mock_user_services.side_effect = [self.sender]*2
#
#         with patch("services.messages_services.database") as mock_db:
#             mock_db.get_user_by_id.side_effect = [self.sender, self.receiver] # not really good idea
#             mock_db.read_query.return_value = self.msg
#
#             result = messages_services.get_conversation_with_user(self.receiver.id,
#                                                                   self.logged_user,
#                                                                   as_txt=False,
#                                                                   txt_width=50)
#             expected = [Message.msg_create(*row[:2]) for row in self.msg]
#
#             self.assertEqual(result, expected)
#
#     def test_create_msg_with_correct_params( self ):
#         user_services.get_user_by_id.side_effect = [self.receiver, self.sender]
#         with patch("services.messages_services.database") as mock_db:
#             mock_db.insert_query.side_effect = ["", ""]
#
#             result = messages_services.create_message(self.msg_txt,
#                                                       self.logged_user)
#
#             row = (self.receiver.id, self.msg_txt.text, self.sender.id)
#             expected = Message.msg_create(*row[:-1])
#
#             self.assertEqual(result, expected)
#
#     def test_create_msg_with_bad_input( self ):
#         user_services.get_user_by_id.return_value=None
#         with self.assertRaises(HTTPException):
#             messages_services.create_message(self.msg_txt,
#                                              self.logged_user)
#
#     def test_delete_message_by_id_with_correct_params(self):
#         with patch("services.messages_services.database") as mock_db:
#             mock_db.read_query.return_value = self.msg
#             mock_db.delete_put_query.return_value = None
#
#             result  = messages_services.delete_message_by_id(1, self.logged_user)
#             expected = DBMessage.msg_create(*self.msg[0])
#
#             self.assertEqual(result, expected)
#
#
# if __name__ == '__main__':
#     unittest.main()