import uvicorn

from fastapi import FastAPI
from routers.auth import auth_router
from routers.user import user_router
# from routers.messages import messages_router
from routers.reply_routes import reply_router
from routers.topic_routers import topics_router
from routers.category_routers import categories_router
from routers.private_categories import private_categories_router
from routers.votes_routesr import vote_router

app = FastAPI()
# import routers
app.include_router(auth_router)
app.include_router(user_router)
# app.include_router(messages_router)
app.include_router(topics_router)
app.include_router(reply_router)
app.include_router(categories_router)
app.include_router(private_categories_router)
app.include_router(vote_router)

if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True, log_level="debug")
