import sys

from typing import List, Iterable
# from collections import Iterable

from mariadb import connect
from mariadb.connections import Connection
from mariadb import Error


def _get_connection() -> Connection:
    try:
        return connect(
            user="root",
            password='Krisko2606@',
            host='127.0.0.1',
            port=3306,
            database="forum"
        )
    except Error as e:  # this error Handling doesn't work as expected !
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)



def read_query(sql: str, sql_params=()) -> list:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return list(cursor)

def delete_query(sql: str, sql_params=()) -> None:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()
    return None




def insert_query(sql: str, sql_params=()) -> int:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.lastrowid


def update_query(sql: str, sql_params=()) -> bool:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

    return True



def first_row_query(sql: str, sql_params=()):
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        # return list(cursor)
        return cursor.fetchone()


if __name__ == "__main__":
    pass
