from typing import Optional, Union, List

from datetime import date, datetime
from pydantic import BaseModel, root_validator, PositiveInt, EmailStr, constr

# from project.data.utils import construct_fast


class LoginUser(BaseModel):
    username: str
    password: str
    is_admin: bool


class MessagedUser(BaseModel):
    id: int
    username: str
    email: str
    first_name: Optional[str]
    last_name: Optional[str]
    is_admin: bool

    @classmethod
    def create_msg_user(cls, id, username, email, first_name, last_name, is_admin):
        return cls(
            id=id,
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name,
            is_admin=is_admin,
        )


class BaseUser(BaseModel):
    username: str
    email: EmailStr
    password: str  # will be hashed before the DB-insertion
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    phone: Optional[str] = None
    is_admin: bool = False

    # TODO: SHOULD
    # ============
    # read_access: bool = False
    # write_access: bool = False
    #
    # @root_validator
    # def update_creation( cls, values ):
    #     if values["is_admin"]:
    #         values["read_access"] = True
    #         values["write_acess"] = True
    #         return values
    #     return values

    @classmethod
    def create_db_user(
            cls, username, email, password, first_name, last_name, phone, is_admin
    ):
        return cls(
            username=username,
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            is_admin=is_admin,
        )


class DBUser(BaseUser):
    id: int

    @classmethod
    def create_user_from_db_row(
            cls, id, username, email, password, first_name, last_name, phone, is_admin
    ):
        return cls(
            id=id,
            username=username,
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            is_admin=is_admin,
        )


class BaseUserOut(BaseModel):
    username: str
    email: str
    is_admin: bool
    # read_access: bool = False
    # write_access: bool = False


class UserOut(BaseUserOut):
    created_at: datetime = datetime.now()

    @root_validator
    def update_creation(cls, values):
        values["created_at"] = datetime.now().strftime("%d/%m/%y %H:%M")
        return values


class Category(BaseModel):
    id: Optional[PositiveInt]
    name: Optional[str]
    is_private: Optional[bool]
    is_locked: Optional[bool]

    @classmethod
    def category(cls, id, name, is_private, is_locked):
        return cls(id=id, name=name, is_private=is_private, is_locked=is_locked)


class Topic(BaseModel):
    id: Optional[PositiveInt]
    title: Optional[str]
    description: Optional[str]
    create_date: Optional[datetime]
    category_id: Optional[PositiveInt]
    user: Optional[Union[int, str]]
    is_locked: Optional[bool]

    @classmethod
    def from_query_result(cls, id, title, description, create_date, category, user, is_locked):
        return cls(id=id, title=title, description=description,
                   create_date=create_date, category_id=category, user=user, is_locked=is_locked)


class Reply(BaseModel):
    id: Optional[int]
    text: str
    create_date: Optional[datetime]
    topic_id: Optional[int]
    user_id: Optional[int]
    votes: Optional[int]
    best_reply: Optional[int]

    @classmethod
    def from_query_result(
            cls, id, text, create_date, topic_id, user_id, votes, best_reply
    ):
        return cls(
            id=id,
            text=text,
            create_date=create_date,
            topic_id=topic_id,
            user_id=user_id,
            votes=votes,
            best_reply=best_reply,
        )


class Createreply(BaseModel):
    id: Optional[int]
    text: str
    create_date: Optional[datetime]
    topic_id: Optional[int]
    user_id: Optional[int]

    @classmethod
    def from_query_result(cls, id, text, create_date, topic_id, user_id):
        return cls(
            id=id,
            text=text,
            create_date=create_date,
            topic_id=topic_id,
            user_id=user_id,
        )


class ReplyUpdate(BaseModel):
    text: str



class Vote(BaseModel):
    user_id: int
    reply_id: int
    vote: int

    @classmethod
    def from_query_result(cls, user_id, reply_id, vote):
        return cls(user_id=user_id, reply_id=reply_id, vote=vote)


class Message(BaseModel):
    id: Union[int, str]
    text: str

    @classmethod
    def msg_create(cls, id, text):
        return cls(id=id, text=text)


class DBMessage(BaseModel):
    id: int
    text: str
    create_date: datetime
    sender_id: int
    receiver_id: int

    # @root_validator
    # def update_creation(cls, values):
    #     values["create_date"] = values["create_date"].strftime("%d/%m/%y %H:%M")
    #     return values

    # @construct_fast
    @classmethod
    def msg_create(cls, id, text, create_date, sender_id, receiver_id):
        return cls(
            id=id,
            text=text,
            create_date=create_date,
            sender_id=sender_id,
            receiver_id=receiver_id,
        )


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    id: int
    username: str
    is_admin: bool
    # read_access:bool
    # write_access:bool


class TopicResponse(BaseModel):
    id: Optional[PositiveInt]
    title: str
    description: str
    date: Optional[datetime]
    category: str
    user: Optional[str]
    is_locked: Optional[bool]
    replies: Optional[List[Reply]]

    @classmethod
    def topic_response(cls, id, title, description, date, category, user, is_locked, replies=[]):
        return cls(id=id, title=title, description=description, date=date, category=category,
                   user=user, is_locked=is_locked, replies=replies)


class TopicResponseForCategory(BaseModel):
    id: Optional[PositiveInt]
    title: str
    description: str
    create_date: Optional[datetime]
    username: str

    @classmethod
    def for_category(cls, id, title, description, create_date, username):
        return cls(id=id, title=title, description=description,
                   create_date=create_date, username=username)


class TopicResponseByUsername(BaseModel):
    id: Optional[PositiveInt]
    title: str
    description: str
    date: Optional[datetime]
    category: str
    username: str

    @classmethod
    def respondbyusername(cls, id, title, description, date, category, username):
        return cls(
            id=id,
            title=title,
            description=description,
            date=date,
            category=category,
            username=username,
        )


# if __name__ == '__main__':
#     pass
# print(User.create_user_from_db_row(1, "Vesko", "blaaa@asdf.com", "asdf", "Vesko", "Tsvetanov", "+35988800000", 0))


class PrivateCategories(BaseModel):
    user_id: PositiveInt
    category_id: PositiveInt
    read_access: Optional[bool]
    write_access: Optional[bool]

    @classmethod
    def get_private(cls, user_id, category_id, read_access, write_access):
        return cls(user_id=user_id, category_id=category_id, read_access=read_access, write_access=write_access)


class PrivateCategoryResponse(BaseModel):
    username: str
    read_access: bool
    write_access: bool

    @classmethod
    def from_query(cls, username, read_access, write_access):
        return cls(username=username, read_access=read_access, write_access=write_access)


class PrivateCategoryUserResponse(BaseModel):
    category: str
    read_access: bool
    write_access: bool

    @classmethod
    def from_query(cls, category, read_access, write_access):
        return cls(category=category, read_access=read_access, write_access=write_access)


