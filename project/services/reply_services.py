from project.data.models import Reply, Topic, ReplyUpdate, BaseUser, Createreply, Category
from project.data.database import insert_query, read_query, update_query
from typing import List


def all():
    data = read_query(
        "SELECT id, text, create_date, topic_id, user_id, votes,  best_reply"
        " FROM replies order by id "
    )

    return (Reply.from_query_result(*row) for row in data)


def get_by_id(id: int):
    data = read_query(
        """SELECT id, text, create_date, topic_id, user_id, votes, best_reply
                             FROM replies WHERE id = ?""",
        (id,),
    )

    return next((Reply.from_query_result(*row) for row in data), None)


def get_by_topic(topic_id: int):
    data = read_query(
        """SELECT id,text,create_date,topic_id,user_id,votes, best_reply
                         FROM replies
                         WHERE  topic_id = ?""",
        (topic_id,),
    )
    return (Reply.from_query_result(*row) for row in data)



def exist(id: int):
    return any(
        read_query(
            """SELECT id,topic_id,user_id,text,create_date,best_reply,votes 
                             FROM replies where id = ?""",
            (id,),
        )
    )


def create(reply: Createreply):
    generated_id = insert_query(
        "INSERT into replies(text, topic_id, user_id) " "VALUES(?,?,?)",
        (reply.text, reply.topic_id, reply.user_id),
    )

    reply.id = generated_id

    return reply


def create_response_object(topic: Topic, topic_replies: List[Reply]):
    return {"topic": topic, "replies": topic_replies}


def update(id: int, reply: ReplyUpdate):
    result = update_query(
        """UPDATE replies SET
           text = ?
           WHERE id = ? 
        """,
        (reply.text, id),
    )

    return result


def delete(id: int):
    update_query("DELETE FROM votes WHERE reply_id = ?", (id,))
    return update_query("DELETE FROM replies WHERE id = ?", (id,))


def update_vote(id: int, vote: int):

    return update_query("UPDATE replies SET votes = ? WHERE id = ?", (vote, id))

# def owns_reply(user: logged_user: TokenData, reply: Reply) -> bool:
#     return order.id in user.orders
def get_category_by_reply(id:int):
    data=read_query('''SELECT c.id, c.name, c.is_private, c.is_locked "
                    "FROM categories as c
                     JOIN topics as t on c.id=t.category_id
                     WHERE t.id in (
                     SELECT topic_id FROM replies
                     WHERE replies.id = ?)''',(id,))

    return next((Category(id=id, name=name, is_private=is_private, is_locked=is_locked)
                 for id, name, is_private, is_locked in data), None)
