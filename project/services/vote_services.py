from project.data.models import Reply
from project.data.database import insert_query, read_query, update_query
from typing import List
from project.data.models import Vote


def get_vote(user_id: int, reply_id: int):
    data = read_query('''
        SELECT user_id, reply_id, vote
        FROM votes WHERE user_id = ? AND reply_id = ?;''',
                      (user_id, reply_id))

    return Vote.from_query_result(*data[0]) if data else None



def create_vote2(user_id: int, reply_id: int, reaction: str):

    the_vote = Vote(user_id=user_id, reply_id=reply_id, vote=1 if reaction == 'like' else -1)

    insert_query('''INSERT INTO votes(user_id, reply_id, vote)
                    VALUES(?,?,?);''',
                 (the_vote.user_id, the_vote.reply_id, the_vote.vote))

    return the_vote


def increment_vote2(the_vote: Vote, reaction: str):
    if reaction == 'like':
        the_vote.vote = 1
    elif reaction == 'dislike':
        the_vote.vote = -1

    update_query('''UPDATE votes SET vote = ?
                       WHERE user_id = ? AND reply_id = ?;''',
                 (the_vote.vote, the_vote.user_id, the_vote.reply_id))

    return the_vote
