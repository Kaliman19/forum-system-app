from jose import JWSError, jwt
from datetime import datetime, timedelta
from fastapi import Depends, status, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

from project.data.database import read_query
from project.data import utils
from project.data.models import DBUser, Token, TokenData


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")  # endpoint to the login-place


SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 600  # [minutes]


def create_token(data: dict):  # data:id,username,is_admin

    to_encode = data.copy()

    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)

    to_encode.update({"exp": expire})

    return jwt.encode(
        to_encode, SECRET_KEY, algorithm=ALGORITHM
    )


def verify_access_toke(token: str, credentials_exception):
    try:

        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])

        id: int = payload.get("id")
        username: str = payload.get("username")
        is_admin: bool = payload.get("is_admin")

        if not username:
            raise credentials_exception
        token_data = TokenData(id=id, username=username, is_admin=is_admin)
    except JWSError:
        raise credentials_exception

    return token_data


def get_current_user(token: str = Depends(oauth2_scheme),) -> TokenData:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    return verify_access_toke(token, credentials_exception)


def create_access_token(user_credential: OAuth2PasswordRequestForm) -> Token:

    select = """SELECT * FROM users
                WHERE users.username=?"""

    # OAuth2PasswordRequestForm --> can be accessed only by .username and .password
    row = read_query(select, (user_credential.username,))

    db_user = (
        DBUser.create_user_from_db_row(*row[0]) if row else None
    )  # we get all info about this user

    if not db_user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found in the DB"
        )

    if not utils.verify(user_credential.password, db_user.password):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Invalid Credentials"
        )

    access_token = create_token(
        {"id": db_user.id, "username": db_user.username, "is_admin": db_user.is_admin}
    )
    return Token(access_token=access_token, token_type="bearer")
