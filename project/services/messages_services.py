# from typing import Union, List
# from operator import itemgetter
# from fastapi import HTTPException, status
#
# import textwrap
#
# #from data.database import insert_query, read_query, delete_put_query
# from data import database
# from data.models import MessagedUser, Message, DBMessage, TokenData, DBUser
#
# #import services.user_services as us
# from services import user_services
#
#
# def get_all_messaged_users(logged_user: TokenData, get_func = None) -> List[MessagedUser]:
#
#     sender = user_services.get_user_by_id(logged_user.username)
#
#     # dependency injection:
#     if get_func is None:
#         select_receivers = """SELECT id, username, email, first_name, last_name, is_admin
#                               FROM users
#                               WHERE id IN (SELECT receiver_id
#                                            FROM conversations
#                                            WHERE sender_id = ?);"""
#
#         rows = database.read_query(select_receivers, (sender.id,))
#     else:
#         rows = get_func(sender.id)
#     return [MessagedUser.create_msg_user(*row) for row in rows]
#
#
# def get_conversation_with_user(
#     id: Union[str, int],
#         logged_user: TokenData, as_txt: bool, txt_width: int
# ) -> Union[List[Message], str]:
#
#     sender: DBUser = user_services.get_user_by_id(logged_user.username)
#     receiver: DBUser = user_services.get_user_by_id(id)
#
#     select = """SELECT * FROM messages
#                 WHERE (sender_id=? AND receiver_id=?) OR (sender_id=? AND receiver_id=?)
#                 ORDER BY id;
#              """
#     params = (sender.id, receiver.id, receiver.id, sender.id)
#     rows = database.read_query(select, params)
#
#     # in dev
#     if as_txt:
#         # under construction
#         def helper(rows):
#             for row in rows:
#                 msg_id, send_on, sender_id, txt = row
#                 tmp = (
#                     f"{msg_id:>5}  "
#                     f"{send_on.strftime('%d/%m/%y %H:%M'):>15} "
#                     f"{sender_id:>5} "
#                     f"%s"
#                 )
#                 yield tmp % textwrap.fill(txt, width=txt_width) # <-- TODO: test the correctness of this peace
#
#         txt_rows = (itemgetter(0, 2, 3, 1)(row) for row in rows)
#         #print("\n".join(i for i in helper(txt_rows))) # <-- TODO: delete me after the acceptance of the changes
#         return "\n".join(i for i in helper(txt_rows))
#     else:
#         return [Message.msg_create(*row[:2]) for row in rows]
#
#
# def create_message(msg_txt: Message, logged_user: TokenData) -> Message:
#
#     receiver = user_services.get_user_by_id(msg_txt.id)
#
#     if not receiver:
#         raise HTTPException(
#             status_code=status.HTTP_404_NOT_FOUND,
#             detail=f"Receiver not found {msg_txt}",
#         )
#
#     sender = user_services.get_user_by_id(logged_user.username)
#
#     insert_users_ids_middle_table = """INSERT INTO conversations
#                                        VALUES (?,?);"""
#
#     try:
#         database.insert_query(insert_users_ids_middle_table, (sender.id, receiver.id))
#     except Exception as e:
#         print(f"Unexpected error: {e}")
#         pass
#
#     insert_msg = """INSERT INTO messages (receiver_id, text , sender_id)
#                     VALUES(?, ?, ?)"""
#
#     row = (receiver.id, msg_txt.text, sender.id)
#     database.insert_query(insert_msg, row)
#
#     return Message.msg_create(*row[:-1])
#
#
# def update_msg(msg: Message, logged_user: TokenData) -> DBMessage:
#     update_msg = """UPDATE messages
#                     SET text=?
#                     WHERE id=?;"""
#
#     database.update_query(update_msg, (msg.text, msg.id))
#
#     return_msg = """SELECT * FROM messages
#                     WHERE id=?;"""
#
#     msg_row = database.read_query(return_msg, (msg.id,))
#
#     return DBMessage.msg_create(*msg_row[0]) if msg_row else None
#
#
# def delete_message_by_id(id: int, logged_user: TokenData) -> Union[DBMessage, None]:
#     select = """SELECT * FROM messages
#                 WHERE id=?"""
#     row = database.read_query(select, (id,))
#
#     if row is None or logged_user.id != row[0][-2]:
#         return None
#     else:
#         del_query = """DELETE FROM messages
#                        WHERE messages.id=?"""
#         database.update_query(del_query, (id,))
#         return DBMessage.msg_create(*row[0])
#
#
# if __name__ == "__main__":
#     pass
#
