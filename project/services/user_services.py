from typing import Union, List
from fastapi import status, HTTPException  # , Depends

# from data.database import insert_query, read_query    # commented out because of the mocking tests
from project.data import database                               # the Mock work's this way
from project.data.models import BaseUser, DBUser, TokenData


def get_user_by_id(id: Union[str, int], get_func=None) -> Union[DBUser, None]:
    if not isinstance(id, (str, int)):
        return None
    else:
        if isinstance(id, str):
            select = """SELECT * FROM users
                        WHERE users.username=?"""
        elif isinstance(id, int):
            select = """SELECT * FROM users
                        WHERE users.id=?"""
        # dependency injection:
        if get_func is None:
            row = database.read_query(select, (id,))
        else:
            row = get_func(id)

        return DBUser.create_user_from_db_row(*row[0]) if row else None


def user_exists(id: Union[str, int], get_user_func=None) -> bool:
    # dependency injection:
    if get_user_func is None:
        user = get_user_by_id(id)
    else:
        user = get_user_func(id)
    return True if user else False


def create_user(
    user: BaseUser, insert_func=None, get_row_func=None
) -> Union[DBUser, None]:

    vals = [v for v in user.dict().values()]

    # dependency injection:
    if insert_func is None:
        insert_user_sql = """INSERT INTO users (username, email, password, first_name, last_name, phone, is_admin)
                                 VALUES (?, ?, ?, ?, ?, ?, ?);"""
        id = database.insert_query(insert_user_sql, vals)
    else:
        id = insert_func(vals)

    if get_row_func is None:
        select_sql = """SELECT * FROM users
                       WHERE users.id=?;"""
        rows = database.read_query(select_sql, (id,))
    else:
        rows = get_row_func(id)

    return DBUser.create_user_from_db_row(*rows[0]) if rows else None


def get_all_users(logged_user: TokenData, get_all=None) -> List[DBUser]:

    if logged_user.is_admin:
        # dependency injection:
        if get_all is None:
            select_all = """SELECT * FROM users"""
            rows = database.read_query(select_all)
        else:
            rows = get_all()
        return [DBUser.create_user_from_db_row(*row) for row in rows]
    else:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="No admin rights;Access denied",
        )
