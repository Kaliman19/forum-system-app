from project.data.database import read_query, update_query, first_row_query
from project.data.models import PrivateCategories, PrivateCategoryResponse, PrivateCategoryUserResponse


def get_categories():
    data = read_query("SELECT DISTINCT * FROM private_categories")

    return (PrivateCategories.get_private(*row) for row in data)


def get_categories_by_user_id(id: int):
    data = read_query("SELECT c.name, pr.read_access, pr.write_access FROM private_categories pr"
                      " JOIN categories c on c.id = pr.categories_id"
                      " WHERE pr.users_id = ?", (id,))

    return (PrivateCategoryUserResponse.from_query(*row) for row in data)


def get_users_by_category_id(id: int):
    data = read_query("SELECT u.username, pr.read_access, pr.write_access FROM private_categories pr"
                      " JOIN users u on pr.users_id = u.id"
                      " WHERE pr.categories_id = ?", (id,))

    return (PrivateCategoryResponse.from_query(*row) for row in data)


def check_if_user_has_access_to_category(category_id: int, user_id: int):
    data = first_row_query("SELECT * from private_categories WHERE categories_id = ?"
                           " and users_id = ?", (category_id, user_id))

    if data:
        return PrivateCategories.get_private(*data)
    # return len(data) > 0


def add_user_to_private_category(category_user: PrivateCategories):
    data = update_query("INSERT INTO private_categories(users_id, categories_id, read_access, write_access)"
                        " VALUES(?,?,?,?)",
                        (category_user.user_id, category_user.category_id,
                         category_user.read_access, category_user.write_access))

    return data


def update_category(old: PrivateCategories, new: dict):
    for k, v in new.items():
        if k == 'read_access' and v is not None:
            old.read_access = v

        elif k == 'write_access' and v is not None:
            old.write_access = v

    data = update_query("UPDATE private_categories SET read_access = ?, write_access = ?"
                        " WHERE users_id = ? and categories_id = ?",
                        (old.read_access, old.write_access, old.user_id, old.category_id))

    return data


def delete_category(id: int):
    return update_query("DELETE FROM private_categories WHERE categories_id = ?", (id,))


