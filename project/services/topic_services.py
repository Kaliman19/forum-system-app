from project.data.models import TopicResponse, Topic, TopicResponseForCategory, TopicResponseByUsername, TokenData
from project.data.database import read_query, update_query, first_row_query, insert_query
from typing import List


def all_topics(search: str = None):
    if not search:
        data = read_query("SELECT t.id, t.title, t.description, t.create_date, c.name,"
                          " u.username, t.is_locked FROM topics t"
                          " JOIN users u on u.id = t.user_id"
                          " JOIN categories c on t.category_id = c.id"
                          " ORDER BY t.create_date")

    else:
        data = read_query("SELECT t.id, t.title, t.description, t.create_date, c.name,"
                          " u.username, t.is_locked FROM topics t"
                          " JOIN users u on u.id = t.user_id "
                          " JOIN categories c on t.category_id = c.id"
                          " WHERE t.title LIKE ?",
                          (f'%{search}%',))

    return (TopicResponse.topic_response(*row) for row in data)


def get_by_id(id: int):
    data = read_query("SELECT t.id, t.title, t.description, t.create_date, c.name,"
                      " u.username, t.is_locked FROM topics t"
                      " JOIN categories c on t.category_id = c.id"
                      " JOIN users u on u.id = t.user_id WHERE t.id = ?", (id,))

    return next((TopicResponse.topic_response(*row) for row in data), None)


def get_by_category(category_id: int):
    data = read_query("SELECT t.id, t.title, t.description, t.create_date, u.username FROM topics t "
                      " JOIN categories c on c.id = t.category_id "
                      " JOIN users u on t.user_id = u.id WHERE category_id = ?", (category_id,))

    return (TopicResponseForCategory.for_category(*row) for row in data)


def create(topic: Topic, logged_user: TokenData):
    generate_topic = insert_query(
        "INSERT INTO topics(title, description, category_id, user_id, is_locked) VALUES(?,?,?,?,?)",
        # " RETURNING id, title, description, create_date, category_id, user_id, is_locked",
        (topic.title, topic.description, topic.category_id, logged_user.id, topic.is_locked))
    topic.id=generate_topic

    # return Topic.from_query_result(*generate_topic)
    return topic


def to_sort(topic: List[Topic], *, attribute="time", reverse=False):
    if attribute == "time":
        result = sorted(topic, key=lambda t: t.create_date, reverse=reverse)

    elif attribute == "title":
        result = sorted(topic, key=lambda t: t.title, reverse=reverse)

    else:
        result = sorted(topic, key=lambda t: t.id, reverse=reverse)

    return result


def exist(id: int) -> bool:
    data = read_query("SELECT * FROM topics WHERE id = ?", (id,))
    return len(data) > 0


def delete(topic: TopicResponse):
    update_query(f"DELETE FROM votes WHERE reply_id in (SELECT id FROM replies WHERE topic_id = ?)", (topic.id,))
    update_query("DELETE FROM replies WHERE topic_id = ?", (topic.id,))
    return update_query("DELETE FROM topics WHERE id = ?", (topic.id,))


def update(old: Topic, new: dict):
    for k, v in new.items():
        if k == 'description' and v is not None:
            old.description = v
        elif k == 'is_locked' and v is not None:
            old.is_locked = v

    return update_query(
        "UPDATE topics SET description = ?, is_locked = ?  WHERE id = ?",
        (old.description, old.is_locked, old.id)
    )


def all_topics_by_user(search: str = None):
    data = read_query(
        "SELECT t.id, t.title, t.description, t.create_date, c.name, u.username FROM topics t"
        " JOIN users u on u.id = t.user_id"
        " JOIN categories c on c.id = t.category_id"
        " WHERE t.user_id = u.id and u.username = ? ",
        (search,),
    )

    return (TopicResponseByUsername.respondbyusername(*row) for row in data)
