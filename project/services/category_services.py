from project.data.models import Category, TokenData
from project.data.database import insert_query, read_query, update_query
from project.services import topic_services


def all_categories(search: str = None):
    if not search:
        data = read_query('SELECT id, name, is_private, is_locked FROM categories ORDER BY id')

    else:
        data = read_query('SELECT id, name, is_private, is_locked FROM categories WHERE name LIKE ?', (f'%{search}%',))

    return (Category.category(*row) for row in data)


def get_by_id(id: int):
    data = read_query(
        "SELECT id, name, is_private, is_locked FROM categories WHERE id = ?", (id,)
    )

    return next((Category.category(*row) for row in data), None)


def get_by_name(name: str):
    data = read_query(
        "SELECT id, name, is_private, is_locked FROM categories WHERE name = ?", (name,)
    )

    return next((Category(id=id, name=name, is_private=is_private, is_locked=is_locked)
                 for id, name, is_private, is_locked in data), None)


def exist(id: int) -> bool:
    data = read_query('SELECT id, name FROM categories WHERE id = ?', (id,))
    return len(data) > 0


def exist_name(category_name: str) -> bool:
    data = read_query('SELECT id, name FROM categories WHERE name = ?', (category_name,))
    return len(data) > 0


#TODO: logged_user isn't used
def create(category: Category, logged_user: TokenData):
    generate_id = insert_query('INSERT INTO categories(name, is_private, is_locked) values(?, ?, ?)',
                               (category.name, category.is_private, category.is_locked))

    category.id = generate_id
    return category


def delete(id: int):
    topics = topic_services.get_by_category(id)
    topic_ids = ','.join(str(t.id) for t in topics)
    if topic_ids:
        update_query(f"DELETE FROM votes WHERE reply_id in "
                     f"(SELECT id FROM replies r WHERE r.topic_id in({topic_ids}))")
        update_query(f"DELETE FROM replies WHERE topic_id in ({topic_ids})")
        update_query("DELETE FROM topics WHERE category_id = ?", (id,))
    update_query("DELETE FROM private_categories WHERE categories_id = ?", (id,))
    update_query("DELETE FROM categories WHERE id = ?", (id,))


def update(existing_category: Category, category: dict):

    for k, v in category.items():
        if k == 'is_private' and v is not None:
            existing_category.is_private = v
        elif k == 'is_locked' and v is not None:
            existing_category.is_locked = v

    update_query("UPDATE categories SET is_private = ?, is_locked = ? WHERE id = ?",
                 (existing_category.is_private, existing_category.is_locked, existing_category.id))

    return existing_category
