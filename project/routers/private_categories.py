from typing import List

from fastapi import APIRouter, Response, Depends, HTTPException, status
from project.data.models import TokenData,PrivateCategories, BaseModel, PrivateCategoryResponse, \
    PrivateCategoryUserResponse
from project.services.auth_services import get_current_user
from project.services.private_categories_services import get_categories, get_categories_by_user_id, \
    check_if_user_has_access_to_category, add_user_to_private_category, update_category, get_users_by_category_id, \
    delete_category
from project.services import category_services, user_services

private_categories_router = APIRouter(prefix='/private_categories', tags=['Private Categories'])


class ResponseCategory(BaseModel):
    category: str
    users: List[PrivateCategoryResponse]


class ResponseUser(BaseModel):
    user: str
    private_categories: List[PrivateCategoryUserResponse]


@private_categories_router.get('/')
def get_private_categories(logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    data = list(get_categories())

    if not data:
        return Response(status_code=404, content="Category does not exist.")

    return data


@private_categories_router.get('/{id}/user')
def get_private_categories_by_user(id: int, logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    user = user_services.get_user_by_id(id)

    if not user:
        return Response(status_code=404, content="User does not exist.")

    data = get_categories_by_user_id(user.id)
    return ResponseUser(user=user.username, private_categories=data)


@private_categories_router.get('/{id}/category', response_model=ResponseCategory)
def get_users_of_a_category(id: int, logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
    category = category_services.get_by_id(id)

    if not category:
        return Response(status_code=404, content="Category does not exist.")

    data = get_users_by_category_id(category.id)

    return ResponseCategory(category=category.name, users=data)


@private_categories_router.post("/")
def add_private_category_and_user(category_user: PrivateCategories,
                                  logged_user: TokenData = Depends(get_current_user)):
    current_category = category_services.get_by_id(category_user.category_id)
    current_user = user_services.get_user_by_id(category_user.user_id)

    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    if not current_category:
        return Response(status_code=404, content="Category does not exist.")

    if not current_user:
        return Response(status_code=404, content="User does not exist.")

    if check_if_user_has_access_to_category(current_category.id, current_user.id):
        raise HTTPException(status_code=status.HTTP_409_CONFLICT)

    if add_user_to_private_category(category_user):
        return Response(status_code=status.HTTP_201_CREATED)


@private_categories_router.put("/update_category")
def update_private_access(new_access: PrivateCategories, logged_user: TokenData = Depends(get_current_user)):

    if not logged_user.is_admin:
        return Response(status_code=status.HTTP_401_UNAUTHORIZED)

    access = check_if_user_has_access_to_category(new_access.category_id, new_access.user_id)

    if not access:
        return Response(status_code=status.HTTP_404_NOT_FOUND)

    if update_category(access, new_access.dict()):
        return {'message': f'Private category updated!'}


@private_categories_router.delete("/{id}/delete")
def delete_private_category(id: int, logged_user: TokenData = Depends(get_current_user)):
    category = category_services.get_by_id(id)

    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    if not category:
        return Response(status_code=404, content="Category does not exist.")

    if delete_category(id):
        return Response(status_code=status.HTTP_204_NO_CONTENT)
