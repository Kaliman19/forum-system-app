from fastapi import APIRouter, Response, Depends, HTTPException, status
from project.data.models import Topic, TopicResponse, TokenData
from project.services import category_services, topic_services, reply_services, private_categories_services
from project.services.auth_services import get_current_user
from typing import Optional

topics_router = APIRouter(prefix="/topics", tags=['Topics'])


@topics_router.get("/")
def get_topics(
        sort: Optional[str] = None,
        sort_by: Optional[str] = None,
        search: Optional[str] = None,
        logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    result = topic_services.all_topics(search)

    if sort and (sort == "asc" or sort == "desc"):
        return topic_services.to_sort(result, reverse=sort == "desc", attribute=sort_by)

    return (TopicResponse.topic_response(r.id, r.title, r.description, r.date, r.category,
                                         r.user, r.is_locked) for r in result)


@topics_router.get('/{id}', response_model=TopicResponse)
def topic_by_id(id: int, logged_user: TokenData = Depends(get_current_user)):
    data = topic_services.get_by_id(id)
    if not data:
        return Response(status_code=404, content="Topic does not exist.")

    category = category_services.get_by_name(data.category)

    if category.is_private and not logged_user.is_admin:
        check_access = private_categories_services.check_if_user_has_access_to_category(category.id, logged_user.id)

        if not check_access or not check_access.read_access:
            return Response(status_code=status.HTTP_401_UNAUTHORIZED)

    replies = reply_services.get_by_topic(data.id)
    data.replies = replies

    return TopicResponse.topic_response(data.id, data.title, data.description, data.date, data.category,
                                        data.user, data.is_locked, replies)


@topics_router.post('/', status_code=201)
def create_topic(topic: Topic, logged_user: TokenData = Depends(get_current_user)):
    category = category_services.get_by_id(topic.category_id)
    if not category:
        return Response(status_code=status.HTTP_404_NOT_FOUND,
                        content=f'Category {topic.category_id} does not exist')
    if category.is_locked:
        return Response(status_code=status.HTTP_423_LOCKED, content="The resource that is being accessed is locked.")

    if category.is_private and not logged_user.is_admin:
        check_access = private_categories_services.check_if_user_has_access_to_category(category.id, logged_user.id)

        if not check_access or not check_access.write_access:
            return Response(status_code=status.HTTP_401_UNAUTHORIZED)

    return topic_services.create(topic, logged_user)


@topics_router.delete('/{id}')
def delete_topic(id: int, logged_user: TokenData = Depends(get_current_user)):
    data = topic_services.get_by_id(id)
    if not data:
        return Response(status_code=404, content="Topic does not exist.")

    if logged_user.is_admin or data.user == logged_user.username:
        topic_services.delete(data)
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    return Response(status_code=401, content="You are not allowed to delete this topic!")


@topics_router.put('/{id}')
def update_topic(id: int, topic: Topic,logged_user: TokenData = Depends(get_current_user)):
    data = topic_services.get_by_id(id)
    if not data:
        return Response(status_code=404, content="Topic does not exist.")

    if logged_user.is_admin or data.user == logged_user.username:
        try_to_update = topic_services.update(data, topic.dict())

        if try_to_update:
            return {'result': f'Topic with id {id} updated'}

    return Response(status_code=401, content="You are not allowed to update this topic!")
