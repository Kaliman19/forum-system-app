from fastapi import APIRouter, Response, Depends, HTTPException, status
from project.data.models import Reply, Topic, ReplyUpdate, BaseUser, Createreply
from project.services import reply_services, topic_services, category_services, private_categories_services
from project.data.models import TokenData
from project.services.auth_services import get_current_user

reply_router = APIRouter(prefix="/replies", tags=['Replies'])


@reply_router.get("/")
def get_replies(logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
    result = reply_services.all()
    return result


@reply_router.get("/{id}")
def get_reply_by_id(id: int, logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    if not reply_services.exist(id):
        return Response(
            status_code=400, content=f"Reply with index {id} does not exist"
        )
    reply = reply_services.get_by_id(id)

    if reply is None:
        return Response(status_code=404)
    else:
        return reply


@reply_router.put("/{id}")
def update_reply(id: int, reply: ReplyUpdate, logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    current_reply = reply_services.get_by_id(id)
    if not current_reply:
        return Response(status_code=404)

    if logged_user.id == current_reply.user_id:
        reply_services.update(id, reply)
    else:
        return Response(status_code=401, content="You are not allowed to update this reply!")

    return {"result": f"Reply successfully updated"}


@reply_router.post("/")
def create_reply(reply: Createreply, logged_user: TokenData = Depends(get_current_user)):
    topic = topic_services.get_by_id(reply.topic_id)
    if not topic or topic.is_locked:
        return Response(status_code=403, content=f"Topic {id} does not exist or the topic is locked")

    current_category = category_services.get_by_name(topic.category)
    reply.user_id = logged_user.id

    if current_category.is_private and not logged_user.is_admin:
        pr_category = private_categories_services.check_if_user_has_access_to_category(current_category.id,
                                                                                       logged_user.id)

        if not pr_category or not pr_category.write_access:
            return Response(status_code=403, content='Category is private')

        return reply_services.create(reply)

    return reply_services.create(reply)


@reply_router.delete("/{id}", status_code=204)
def delete_reply(id: int, logged_user: TokenData = Depends(get_current_user)):
    reply = reply_services.get_by_id(id)
    if not reply:
        return Response(status_code=404, content="Reply does not exist.")

    if logged_user.id == reply.user_id or logged_user.is_admin:
        reply_services.delete(id)

    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
