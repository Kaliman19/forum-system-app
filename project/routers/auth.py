from fastapi import APIRouter, Depends
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from project.services.auth_services import create_access_token
from project.data.models import Token

auth_router = APIRouter(tags=["Authentication"])


@auth_router.post("/login", response_model=Token)
def login(user_credential: OAuth2PasswordRequestForm = Depends()):
    return create_access_token(user_credential)


# No needed logout, because otherwise will go against
# the logic of the JWT-mechanic !
# -------------------------------------------------------
# @auth_router.get("/logout", response_model=BaseUserOut)
# def logging_out(logged_user: TokenData):
#     return
