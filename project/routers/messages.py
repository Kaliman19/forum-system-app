# from typing import List, Optional, Union
# from fastapi import APIRouter, Depends, HTTPException, status
# from project.data.models import MessagedUser, Message, DBMessage, TokenData
#
# from project.services.auth_services import get_current_user
#
# import project.services.messages_services as ms
# import project.services.user_services as us
#
#
# messages_router = APIRouter(prefix="/messages", tags=["Messages"])
#
# # print("type(messages_router) --->", type(messages_router).__qualname__)
# # print(type(messages_router).__qualname__ == "APIRouter")
#
#
# @messages_router.get("/conversation/{id}")  # id == user_id or username
# def get_conversation_for_logged_user(
#     id: Union[int, str],
#     as_txt: bool = False,
#     txt_width: int = 50,
#     logged_user: TokenData = Depends(get_current_user),
# ) -> Union[List[Message], str]:
#     return ms.get_conversation_with_user(id, logged_user, as_txt, txt_width)
#
#
# @messages_router.get("/conversations")
# def get_all_messaged_users_for_logged_in(
#     logged_user: TokenData = Depends(get_current_user),
# ) -> List[MessagedUser]:
#     return ms.get_all_messaged_users(logged_user)
#
#
# @messages_router.post("/", response_model=Message)  # id == user_id or username
# def send_message_to_user(
#     msg_txt: Message, logged_user: TokenData = Depends(get_current_user)
# ):
#
#     receiver = us.get_user_by_id(msg_txt.id)  # <-- bad design !
#
#     if receiver.id == logged_user.id:
#         raise HTTPException(
#             status_code=status.HTTP_417_EXPECTATION_FAILED,
#             detail="You can't send yourself messages",
#         )
#
#     return ms.create_message(msg_txt, logged_user)
#
#
# @messages_router.put("/conversations", response_model=DBMessage)
# def update_message_by_id(
#     msg: Message, logged_user: TokenData = Depends(get_current_user)
# ) -> DBMessage:
#
#     return ms.update_msg(msg, logged_user)
#
#
# @messages_router.delete("/{id}", response_model=DBMessage)
# def delete_message_by_id(id: int, logged_user: TokenData = Depends(get_current_user)):
#     tmp = ms.delete_message_by_id(id, logged_user)
#
#     if tmp is not None:
#         return tmp
#     else:
#         raise HTTPException(
#             status_code=status.HTTP_417_EXPECTATION_FAILED,
#             detail="You can't delete messages, which you haven't created,\n"
#             "or the message doesn't exist",
#         )
