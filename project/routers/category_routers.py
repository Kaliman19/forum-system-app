from typing import Optional, List,Union

from fastapi import APIRouter, Response, Depends, HTTPException, status
from pydantic import BaseModel
from project.data.models import Category, Topic, TokenData
from project.services import category_services, topic_services, private_categories_services
from project.services.auth_services import get_current_user
from typing import Optional, List, Union


class CategoryResponseModel(BaseModel):
    category: Category
    topics: Optional[List]


categories_router = APIRouter(prefix='/categories', tags=['Categories'])


@categories_router.get('/', response_model=List[Category])
def get_categories(search: Optional[str] = None, logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    return category_services.all_categories(search)


@categories_router.get('/{id}')
def get_category_by_id(id: int, logged_user: TokenData = Depends(get_current_user)):
    category = category_services.get_by_id(id)
    if not category:
        return Response(status_code=404, content="Category does not exist.")

    if category.is_private:
        check_access = private_categories_services.check_if_user_has_access_to_category(category.id, logged_user.id)

        if logged_user.is_admin or check_access and check_access.read_access:

            return CategoryResponseModel(category=category, topics=topic_services.get_by_category(category.id))

        else:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="You are unauthorized.")

    return CategoryResponseModel(category=category, topics=topic_services.get_by_category(category.id))


@categories_router.post('/')
def create_category(category: Category, logged_user: TokenData = Depends(get_current_user)):
    if not logged_user.is_admin:
        return Response(status_code=status.HTTP_401_UNAUTHORIZED)

    temp = category_services.exist_name(category.name)
    if temp:
        return Response(status_code=400, content=f'Category with name {category.name} exist.')

    new_category = category_services.create(category, logged_user)
    return CategoryResponseModel(category=new_category, topics=[])


@categories_router.delete('/{id}')
def delete_category(id: int, logged_user: TokenData = Depends(get_current_user)):
    if not category_services.get_by_id(id):
        return Response(status_code=404, content=f'Category with id {id} does not exist.')

    if not logged_user.is_admin:
        return Response(status_code=status.HTTP_401_UNAUTHORIZED, content="You are not admin!")

    category_services.delete(id)

    return Response(status_code=204)


@categories_router.put('/{id}')
def update_category(id: int, category: Category, logged_user: TokenData = Depends(get_current_user)):
    existing_category = category_services.get_by_id(id)
    if existing_category is None:
        return Response(status_code=404, content=f'Category with id {id} does not exist.')

    elif not logged_user.is_admin:
        return Response(status_code=403, content="You are not admin!")

    else:
        updated = category_services.update(existing_category, category.dict())
        return CategoryResponseModel(category=updated, topics=topic_services.get_by_category(updated.id))
