from fastapi import APIRouter, Response, Depends, HTTPException, status, Query
from project.services import reply_services, vote_services
from project.data.models import TokenData
from project.services.auth_services import get_current_user
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder


vote_router = APIRouter(prefix="/votes", tags=['Votes'])


@vote_router.put('/{reply_id}')
def reactions(reply_id: int, reaction: str = Query(default=None, regex='^like|dislike|$'),
              logged_user: TokenData = Depends(get_current_user)):
    reply = reply_services.get_by_id(reply_id)
    if not reply:
        return Response(status_code=404)

    the_vote = vote_services.get_vote(logged_user.id, reply_id)

    if the_vote:
        if the_vote.vote == 1 and reaction == 'like':
            return Response(status_code=200, content='You already liked this reply.')
        elif the_vote.vote == -1 and reaction == 'dislike':
            return Response(status_code=200, content='You already disliked this reply.')

        data = vote_services.increment_vote2(the_vote, reaction)
        reply_services.update_vote(reply_id, (reply.votes + data.vote * 2))
        return JSONResponse(status_code=200, content={
            'message': 'Vote updated',
            'data': jsonable_encoder(data)})

    else:
        data = vote_services.create_vote2(logged_user.id, reply_id, reaction)
        reply_services.update_vote(reply_id, (reply.votes + data.vote))
        return JSONResponse(status_code=201, content={
            'message': 'Vote created',
            'data': jsonable_encoder(data)})
