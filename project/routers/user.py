from typing import List, Tuple, Union

from fastapi import status, HTTPException, Depends, APIRouter
from project.data.models import BaseUser, DBUser, BaseUserOut, UserOut, \
                        TokenData, TopicResponseByUsername

from project.services.auth_services import get_current_user

#import services.user_services as us
from project.services import user_services
from project.data.utils import _hash
from project.services import topic_services

# from pprint import pp


user_router = APIRouter(prefix="/users", tags=["Users"])


@user_router.post("/", status_code=status.HTTP_201_CREATED, response_model=UserOut)
def create_user(user: BaseUser) -> DBUser:
    # hash the password - user.password
    hashed_password = _hash(user.password)
    user.password = hashed_password

    db_user = user_services.create_user(user)
    if not db_user:
        raise HTTPException(
            status_code=status.HTTP_417_EXPECTATION_FAILED,
            detail="The new user couldn't be created",
        )

    return db_user


@user_router.get("/", status_code=status.HTTP_200_OK, response_model=List[BaseUserOut])
def get_all_registered_users(logged_user: TokenData = Depends(get_current_user)):
    return user_services.get_all_users(logged_user)


@user_router.get("/{id}", response_model=UserOut)
def get_user(id: Union[int, str]):
    user = user_services.get_user_by_id(id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )
    return user


@user_router.get("/{user_name}/topics")
def get_all_topics_by_user(user_name: Union[str, int],
                           logged_user: TokenData = Depends(get_current_user)):

    if not logged_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="No admin rights;Access denied",
        )

    user = user_services.get_user_by_id(user_name)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not found"
        )

    get_topics = topic_services.all_topics_by_user(user.username)

    return list(get_topics)


