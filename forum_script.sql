-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema forum
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema forum
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `forum` DEFAULT CHARACTER SET latin1 ;
USE `forum` ;

-- -----------------------------------------------------
-- Table `forum`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `is_private` TINYINT(1) NOT NULL DEFAULT 0,
  `is_locked` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `first_name` VARCHAR(45) NULL DEFAULT NULL,
  `last_name` VARCHAR(45) NULL DEFAULT NULL,
  `phone` VARCHAR(45) NULL DEFAULT NULL,
  `is_admin` TINYINT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum`.`conversations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`conversations` (
  `sender_id` INT(11) NOT NULL,
  `receiver_id` INT(11) NOT NULL,
  PRIMARY KEY (`sender_id`, `receiver_id`),
  INDEX `fk_users_has_users_users2_idx` (`receiver_id` ASC),
  INDEX `fk_users_has_users_users1_idx` (`sender_id` ASC),
  CONSTRAINT `fk_users_has_users_users1`
    FOREIGN KEY (`sender_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_users_users2`
    FOREIGN KEY (`receiver_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum`.`messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`messages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(600) NOT NULL,
  `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `sender_id` INT(11) NOT NULL,
  `receiver_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_messages_users_has_users1_idx` (`sender_id` ASC, `receiver_id` ASC),
  CONSTRAINT `fk_messages_users_has_users1`
    FOREIGN KEY (`sender_id` , `receiver_id`)
    REFERENCES `forum`.`conversations` (`sender_id` , `receiver_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum`.`topics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`topics` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(200) NOT NULL,
  `description` VARCHAR(600) NOT NULL,
  `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `category_id` INT(11) NOT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `is_locked` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_topics_categories_idx` (`category_id` ASC),
  INDEX `fk_topics_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_topics_categories`
    FOREIGN KEY (`category_id`)
    REFERENCES `forum`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_topics_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 32
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum`.`replies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`replies` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(600) NOT NULL,
  `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `topic_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `votes` INT(11) NOT NULL DEFAULT 0,
  `best_reply` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_replies_topics1_idx` (`topic_id` ASC),
  INDEX `fk_replies_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_replies_topics1`
    FOREIGN KEY (`topic_id`)
    REFERENCES `forum`.`topics` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_replies_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum`.`votes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`votes` (
  `user_id` INT(11) NOT NULL,
  `reply_id` INT(11) NOT NULL,
  `vote` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`, `reply_id`),
  INDEX `fk_users_has_replies_replies1_idx` (`reply_id` ASC),
  INDEX `fk_users_has_replies_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_users_has_replies_replies1`
    FOREIGN KEY (`reply_id`)
    REFERENCES `forum`.`replies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_replies_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum`.`private_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum`.`private_categories` (
  `users_id` INT(11) NOT NULL,
  `categories_id` INT(11) NOT NULL,
  `read_access` TINYINT(1) NOT NULL DEFAULT 0,
  `write_access` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`users_id`, `categories_id`),
  INDEX `fk_users_has_categories_categories1_idx` (`categories_id` ASC),
  INDEX `fk_users_has_categories_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_users_has_categories_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_categories_categories1`
    FOREIGN KEY (`categories_id`)
    REFERENCES `forum`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
