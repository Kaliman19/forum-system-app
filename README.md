## forum system api


###### Developers: Georgi Dimitrov, Veselin Tsvetanov, Kalin Iliev


### OVERVIEW OF THE PROJECT

The design and implementation of the Forum System, provide RESTful API that can be consumed by different clients.

The business logic has been encapsulate as much as possible in a separate layers. 

The chosen backend solution uses the language Python and the Restful framework - **_FastAPI_** in combination with

relational data base - **_MariaDB_**. For the simulation purposes of the real client-server interaction 

(integrational tests - predominantly by hand) has been used **_POSTMAN_**. The business logic has been covered with 

unittesting. 

The Forum System consists of at least User, Topic, Category, Reply, and Message resources. 

High-level description:

- Users can read and create topics and message other users
- Administrators manage users, topics and categories


The application supports the following operations:

1. User can register, log in and log out. Users can read and create topics, make replies, vote for a reply and send messages to other users.

2. Only administrators can create categories and make them private.

3. All users can create topic if the category is not locked. If the category of the topic is private only users with access and administrators can create a topic.

4. All users can create a reply to a topic if the topic is not locked. If the category of the topic is private only users with access and administrators can create a reply.

5. All users can upvote / downvote a reply.




### 1. USERS

In order to use the forum system, first you must be registered and logged in. In order to create account every user must

write down username, email, password, first name, second name, phone. After the account is created the user must login with his username and password. 

The authentication solutions has been made with the help of the jason web token (JWK) and the protocoll OAuth2.As default value for the expiration time after the first successful login has been set to 10 hours.

-> Every user can send, delete or edit a messages to another user. Also he can view his conversations.

-> The administrators can give or revoke user's access for a private categories.

### 2. MESSAGES

The message system is provided for the personal communication between two users. Every message can be created from logged user to another one and if needed updated and deleted.

### 3. CONVERSATIONS

The conversation system is a subsystem of the message system, which provides the functionallity to give back chronoligacally the communication between two persons.There is another usage of the functionallity and this is to return all users who have been contacted from the logged user.

### 4. CATEGORIES

Only administrators can create categories and make them private and only users with access can read or create topics and make replies. There is a option to lock the category so users can not create topics in that category. If a category is deleted all topics, replies and votes that have relation with the category are also deleted.

### 5. TOPICS

All users can create topics. 
### 6.Replies

Only administrators can get replies by id or all of them.The user that created the reply, can update it, as well as the administrator.If not administrator, user can create reply of a topic, as long as its not locked, otherwise he must have write access to it.Admin or users who created the replies, can delete them.


